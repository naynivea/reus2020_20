package T20_02.T20_02;

/**
 * Hello world!
 *
 */
public class App {
    
	private static final int EXIT_ON_CLOSE = 0;

	public static void main( String[] args ) {
        Vista vista = new Vista();
        
        vista.setVisible(true);
    }
}
