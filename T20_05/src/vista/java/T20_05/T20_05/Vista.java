package T20_05.T20_05;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JTextField;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;
import javax.swing.JTextArea;

public class Vista extends JFrame {

	private JPanel contentPane;

	/**
	 * Create the frame.
	 */
	public Vista() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		final JTextArea textArea = new JTextArea();
		textArea.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				textArea.setText(textArea.getText() + "ratón ha entrado en el componente ");
			}
			@Override
			public void mouseExited(MouseEvent e) {
				textArea.setText(textArea.getText() + "ratón ha salido del componente ");
			}
		});
		textArea.setBounds(0, 45, 434, 216);
		contentPane.add(textArea);
		
		JButton btnBoton = new JButton("Limpiar");
		btnBoton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				textArea.setText("");
			}
		});
		btnBoton.setBounds(179, 11, 89, 23);
		contentPane.add(btnBoton);
	}
}
