package T20_09.T20_09;

import java.awt.Color;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.layout.FormSpecs;
import javax.swing.JToggleButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class Vista extends JFrame {

	private JPanel contentPane;
	private JToggleButton tglbtn1;
	private JToggleButton tglbtn2;
	private JToggleButton tglbtn3;
	private JToggleButton tglbtn4;
	private JToggleButton tglbtn5;
	private JToggleButton tglbtn6;
	private JToggleButton tglbtn7;
	private JToggleButton tglbtn8;
	private JToggleButton tglbtn9;
	private JToggleButton tglbtn10;
	private JToggleButton tglbtn11;
	private JToggleButton tglbtn12;
	private JToggleButton tglbtn13;
	private JToggleButton tglbtn14;
	private JToggleButton tglbtn15;
	private JToggleButton tglbtn16;

	private int contadorClics;
	private int contadorRed;
	private int contadorBlue;
	private int contadorBlack;
	private int contadorGreen;
	private int contadorOrange;
	private int contadorPink;
	private int contadorYellow;
	private int contadorGray;
	/**
	 * Create the frame.
	 */
	public Vista() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 292, 324);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		tglbtn1 = new JToggleButton("");
		tglbtn1.setSelected(true);
		tglbtn1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				tglbtn1.setBackground(Color.RED);
				verificarContadorClics();
			}
		});
		tglbtn1.setBounds(12, 12, 55, 55);
		contentPane.setLayout(null);
		contentPane.add(tglbtn1);
		
		tglbtn2 = new JToggleButton("");
		tglbtn2.setSelected(true);
		tglbtn2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				tglbtn2.setBackground(Color.BLUE);
				verificarContadorClics();
			}
		});
		tglbtn2.setBounds(74, 12, 55, 55);
		contentPane.add(tglbtn2);
		
		tglbtn3 = new JToggleButton("");
		tglbtn3.setSelected(true);
		tglbtn3.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				tglbtn3.setBackground(Color.RED);
				verificarContadorClics();
			}
		});
		tglbtn3.setBounds(136, 12, 55, 55);
		contentPane.add(tglbtn3);
		
		tglbtn4 = new JToggleButton("");
		tglbtn4.setSelected(true);
		tglbtn4.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				tglbtn4.setBackground(Color.BLACK);
				verificarContadorClics();
			}
		});
		tglbtn4.setBounds(198, 12, 55, 55);
		contentPane.add(tglbtn4);
		
		tglbtn5 = new JToggleButton("");
		tglbtn5.setSelected(true);
		tglbtn5.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				tglbtn5.setBackground(Color.green);
				verificarContadorClics();
			}
		});
		tglbtn5.setBounds(12, 74, 55, 55);
		contentPane.add(tglbtn5);
		
		tglbtn6 = new JToggleButton("");
		tglbtn6.setSelected(true);
		tglbtn6.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				tglbtn6.setBackground(Color.BLUE);
				verificarContadorClics();
			}
		});
		tglbtn6.setBounds(74, 74, 55, 55);
		contentPane.add(tglbtn6);
		
		tglbtn7 = new JToggleButton("");
		tglbtn7.setSelected(true);
		tglbtn7.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				tglbtn7.setBackground(Color.ORANGE);
				verificarContadorClics();
			}
		});
		tglbtn7.setBounds(136, 74, 55, 55);
		contentPane.add(tglbtn7);
		
		tglbtn8 = new JToggleButton("");
		tglbtn8.setSelected(true);
		tglbtn8.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				tglbtn8.setBackground(Color.PINK);
				verificarContadorClics();
			}
		});
		tglbtn8.setBounds(198, 74, 55, 55);
		contentPane.add(tglbtn8);
		
		tglbtn9 = new JToggleButton("");
		tglbtn9.setSelected(true);
		tglbtn9.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				tglbtn9.setBackground(Color.YELLOW);
				verificarContadorClics();
			}
		});
		tglbtn9.setBounds(12, 136, 55, 55);
		contentPane.add(tglbtn9);
		
		tglbtn10 = new JToggleButton("");
		tglbtn10.setSelected(true);
		tglbtn10.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				tglbtn10.setBackground(Color.CYAN);
				verificarContadorClics();
			}
		});
		tglbtn10.setBounds(74, 136, 55, 55);
		contentPane.add(tglbtn10);
		
		tglbtn11 = new JToggleButton("");
		tglbtn11.setSelected(true);
		tglbtn11.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				tglbtn11.setBackground(Color.YELLOW);
				verificarContadorClics();
			}
		});
		tglbtn11.setBounds(136, 136, 55, 55);
		contentPane.add(tglbtn11);
		
		tglbtn12 = new JToggleButton("");
		tglbtn12.setSelected(true);
		tglbtn12.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				tglbtn12.setBackground(Color.CYAN);
				verificarContadorClics();
			}
		});
		tglbtn12.setBounds(198, 136, 55, 55);
		contentPane.add(tglbtn12);
		
		tglbtn13 = new JToggleButton("");
		tglbtn13.setSelected(true);
		tglbtn13.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				tglbtn13.setBackground(Color.PINK);
				verificarContadorClics();
			}
		});
		tglbtn13.setBounds(12, 198, 55, 55);
		contentPane.add(tglbtn13);
		
		tglbtn14 = new JToggleButton("");
		tglbtn14.setSelected(true);
		tglbtn14.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				tglbtn14.setBackground(Color.ORANGE);
				verificarContadorClics();
			}
		});
		tglbtn14.setBounds(74, 198, 55, 55);
		contentPane.add(tglbtn14);
		
		tglbtn15 = new JToggleButton("");
		tglbtn15.setSelected(true);
		tglbtn15.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				tglbtn15.setBackground(Color.GRAY);
				verificarContadorClics();
			}
		});
		tglbtn15.setBounds(136, 198, 55, 55);
		contentPane.add(tglbtn15);
		
		tglbtn16 = new JToggleButton("");
		tglbtn16.setSelected(true);
		tglbtn16.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				tglbtn16.setBackground(Color.GRAY);
				verificarContadorClics();
			}
		});
		tglbtn16.setBounds(198, 198, 55, 55);
		contentPane.add(tglbtn16);
	}
	
	private void reiniciarClicks() {
		contadorClics = 0;
		contadorRed = 0;
		contadorBlue = 0;
		contadorBlack = 0;
		contadorGreen = 0;
		contadorOrange = 0;
		contadorPink = 0;
		contadorYellow = 0;
		contadorGray = 0;
	}
	
	private void reiniciarBotones() {
		tglbtn1.setSelected(false);
		tglbtn2.setSelected(false);
		tglbtn3.setSelected(false);
		tglbtn4.setSelected(false);
		tglbtn5.setSelected(false);
		tglbtn6.setSelected(false);
		tglbtn7.setSelected(false);
		tglbtn8.setSelected(false);
		tglbtn9.setSelected(false);
		tglbtn10.setSelected(false);
		tglbtn11.setSelected(false);
		tglbtn12.setSelected(false);
		tglbtn13.setSelected(false);
		tglbtn14.setSelected(false);
		tglbtn15.setSelected(false);
		tglbtn16.setSelected(false);
	}
	
	private void verificarContadorClics() {
		if (contadorClics == 2) {
			reiniciarBotones();
			contadorClics = 0;
		} else {
			contadorClics++;
		}
	}

}
