package T20_07.T20_07;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Vista extends JFrame {

	private JPanel contentPane;
	private JTextField textFieldCantidad;
	private JTextField textFieldResultado;
	private int action;

	/**
	 * Create the frame.
	 */
	public Vista() {
		setTitle("Calculadora cambio de monedas");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblCantidad = new JLabel("Cantidad a convertir");
		lblCantidad.setBounds(22, 11, 117, 27);
		contentPane.add(lblCantidad);
		
		textFieldCantidad = new JTextField();
		textFieldCantidad.setBounds(138, 14, 86, 20);
		contentPane.add(textFieldCantidad);
		textFieldCantidad.setColumns(10);
		
		JLabel lblResultado = new JLabel("Resultado");
		lblResultado.setBounds(250, 14, 64, 21);
		contentPane.add(lblResultado);
		
		textFieldResultado = new JTextField();
		textFieldResultado.setBounds(308, 14, 86, 20);
		contentPane.add(textFieldResultado);
		textFieldResultado.setColumns(10);
		
		action = 1;
		final JButton btnEurosPtas = new JButton("Euros a ptas");
		btnEurosPtas.addActionListener(new ActionListener() { 
			public void actionPerformed(ActionEvent arg0) {
				double cantidad = Double.parseDouble(textFieldCantidad.getText());
				if (action == 1) {
					textFieldResultado.setText(String.valueOf(cantidad * 166.386));
				} else if (action == 0) {
					textFieldResultado.setText(String.valueOf(cantidad / 166.386));
				}
			}
		});
		btnEurosPtas.setBounds(112, 49, 112, 23);
		contentPane.add(btnEurosPtas);
		JButton btnCambiar = new JButton("Cambiar");
		btnCambiar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (action == 0) {
					btnEurosPtas.setText("Euros a ptas");
					action = 1;
				} else if (action == 1) {
					btnEurosPtas.setText("Ptas a Euros");
					action = 0;
				}
			}
		});
		btnCambiar.setBounds(248, 49, 89, 23);
		contentPane.add(btnCambiar);
	}

}
