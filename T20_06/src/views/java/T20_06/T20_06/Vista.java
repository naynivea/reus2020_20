package T20_06.T20_06;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Vista extends JFrame {

	private JPanel contentPane;
	private JTextField textFieldAltura;
	private JTextField textFieldPeso;
	private JButton btnCalcular;
	private JLabel lblIMC;
	private JLabel lblResultado;

	/**
	 * Create the frame.
	 */
	public Vista() {
		setTitle("Indice de Masa Corporal");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 435, 218);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel(" Altura (metros)");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel.setBounds(10, 11, 103, 24);
		contentPane.add(lblNewLabel);
		
		textFieldAltura = new JTextField();
		textFieldAltura.setBounds(110, 15, 86, 20);
		contentPane.add(textFieldAltura);
		textFieldAltura.setColumns(10);
		
		JLabel lblNewLabel_1 = new JLabel("Peso (kg)");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel_1.setBounds(237, 15, 63, 17);
		contentPane.add(lblNewLabel_1);
		
		textFieldPeso = new JTextField();
		textFieldPeso.setBounds(310, 15, 101, 20);
		contentPane.add(textFieldPeso);
		textFieldPeso.setColumns(10);
		
		lblIMC = new JLabel("IMC");
		lblIMC.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblIMC.setBounds(224, 37, 35, 37);
		contentPane.add(lblIMC);
		
		lblResultado = new JLabel("");
		lblResultado.setForeground(Color.BLUE);
		lblResultado.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblResultado.setBounds(258, 43, 80, 21);
		contentPane.add(lblResultado);
		
		btnCalcular = new JButton("Calcular IMC");
		btnCalcular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				double peso = Double.parseDouble(textFieldPeso.getText());
				double altura = Double.parseDouble(textFieldAltura.getText());
				lblResultado.setText(String.format("%.2f", peso / Math.pow(altura, 2)));
			}
		});
		btnCalcular.setBounds(60, 46, 110, 23);
		contentPane.add(btnCalcular);
	}

}
