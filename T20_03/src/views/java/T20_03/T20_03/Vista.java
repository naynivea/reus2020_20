package T20_03.T20_03;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Vista extends JFrame {

	private JPanel contentPane;
	
	/**
	 * Create the frame.
	 */
	public Vista() {
		setTitle("Ventana con más interacción");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 426, 191);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		final JLabel lblLabel1 = new JLabel("");
		lblLabel1.setBounds(69, 11, 118, 35);
		contentPane.add(lblLabel1);
		
		final JLabel lblLabel2 = new JLabel("");
		lblLabel2.setBounds(234, 11, 118, 35);
		contentPane.add(lblLabel2);
		
		JButton btnBoton1 = new JButton("Boton 1");
		btnBoton1.addActionListener(new ActionListener() {
			int contador = 0;
			public void actionPerformed(ActionEvent e) {
				contador++;
				lblLabel1.setText("Boton1: " + contador + " veces");
			}
		});
		btnBoton1.setBounds(79, 57, 89, 35);
		contentPane.add(btnBoton1);
		
		JButton btnBoton2 = new JButton("Boton 2");
		btnBoton2.addActionListener(new ActionListener() {
			int contador = 0;
			public void actionPerformed(ActionEvent e) {
				contador++;
				lblLabel2.setText("Boton2: " + contador + " veces");
			}
		});
		btnBoton2.setBounds(244, 57, 89, 35);
		contentPane.add(btnBoton2);
	}

}
